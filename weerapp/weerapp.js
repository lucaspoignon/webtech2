(function () {
    "use strict"; var Weatherapp = function () {



        this.go = function () {

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(this.showWeather);
            }
        };

        this.showWeather = function (position) {
            console.log(position);

            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
            var apikey = "2a06e31e81dd677f1c945bee198ae74a";
            var url = "https://api.forecast.io/forecast/"+apikey+"/" + latitude + "," + longitude;
            console.log(url);
            $.ajax({
                type: 'GET',
            dataType: "jsonp", //LETTEN OP HOOFDLETTER
            url: url,
            
            success: function (resp) {
                var weather = resp.daily.data;

                $.each(weather, function( dag, inhoud ) {

                var summary = inhoud.summary;
                var temperatureMin = ((inhoud.temperatureMin) - 32) * (5 / 9);
                var temperatureMinRounded = temperatureMin.toFixed(2);
                var temperatureMax = ((inhoud.temperatureMax) - 32) * (5 / 9);
                var temperatureMaxRounded = temperatureMax.toFixed(2);
                var humidity = inhoud.humidity;
                var windSpeed = inhoud.windSpeed;
                var icon = inhoud.icon;
                localStorage.setItem("Weather-week", JSON.stringify(weather));

                var d = new Date(inhoud.time*1000);
                var weekday = new Array(7);
                weekday[0]=  "Zondag";
                weekday[1] = "Maandag";
                weekday[2] = "Dinsdag";
                weekday[3] = "Woensdag";
                weekday[4] = "Donderdag";
                weekday[5] = "Vrijdag";
                weekday[6] = "Zaterdag";
                var datum = weekday[d.getDay()];
                console.log(datum);
                console.log(summary);
                console.log(temperatureMinRounded);
                console.log(temperatureMaxRounded);
                
                $(".container").append(
                "<div class='weerdag'><h1>"+datum+"</h1><h2>Summary</h2><p>"+summary+"</p><h2>Min.Temperature</h2><p>"+temperatureMinRounded+"</p><h2>Max.Temperature</h2><p>"+temperatureMaxRounded+"</p><img src='images/"+icon+".png'></div>"
                );

                // $(document).ready(function () {
                // $("#datum").html(datum);
                // $("#summary").html(summary);
                // $("#temperatureMin").html(temperatureMinRounded);
                // $("#temperatureMax").html(temperatureMaxRounded);
                // $("#humidity").html(humidity);
                // $("#windSpeed").html(windSpeed);

                // });

            })

            },
            error: function () {
                console.log(url);
            }
        });
            };
        }
        var app = new Weatherapp();
        app.go();

    }());
