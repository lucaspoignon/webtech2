# README #

Deze README dient om feedback te geven over de lessen van Webtech2, uit de richting Interactive Multimedia Design. Elke week werd er een ander onderwerp besproken, en je vindt hier een korte samenvatting over het besproken onderwerp.

### Waarvoor dient deze repository? ###

Elke week kregen studenten een taak opgegeven om te vervolledigen tegen de volgende les. Deze repository is aangemaakt zodat de studenten hun werk door middel van GIT kunnen tonen aan de docent. Deze kan controleren wanneer de taken afgeleverd werden. Verder is het ook handig voor de studenten om een back-up te hebben van hun werk.

### Overzicht Onderwerpen ###

# 1. GIT #

Het voornaamste gebruik van GIT is versiecontrole. Grote IT-projecten vereisen vaak een samenwerking van verschillende programmeurs en ontwikkelaars. Door middel van GIT kunnen ontwikkelaars hun eigen branch of tak aanmaken, waarin ze kunnen werken aan hun eigen taak. Nadien als hun code getest is en klaar voor toevoeging, kunnen ze hun werk mergen of samenvoegen met de master-versie. Dit is de versie die op elk ogenblik moet werken. Op deze manier vermijd je problemen als er nieuwe features geïmplementeerd worden.

Ondertussen zijn er verschillende grafische interfaces beschikbaar waarmee je GIT op een eenvoudige manier kan gebruiken. Persoonlijk heb ik met Sourcetree gewerkt. Ik vond dit programma zeer eenvoudig in gebruik en overzichtelijk.

 **Clone:**

Je maakt meestal eerst een repository aan op een webapplicatie zoals Github of Bitbucket. Nadien ga je het mapje dat je online aangemaakt hebt gaan clonen zodat het ook op je computer lokaal aangemaakt wordt. Clonen doe je meestal door een HTTPS-link te verkrijgen en te kopiëren in je lokale client.

 **Branch:**

Branches zijn zeer belangrijk in GIT. Je wil altijd een werkende master versie behouden. Bij elke nieuwe feature die je wil implementeren ga je werken in een aparte branch of tak. Vergelijk het zoals een boom. Zeer belangrijk om te weten is dat als je van branch verandert, de inhoud van je repository of mapje lokaal zich ook aanpast. Dit kan soms verwarrend zijn. Kleine aanpassingen zoals typfoutjes werk je niet uit in een branch maar in de master-versie.

 **Fetch:**
Als je fetcht, dan gaat GIT alle commits van de geselecteerde branch, die niet bestaan in je huidige branch gaan verzamelen. Die slaagt GIT lokaal op. GIT gaat deze commits wel niet automatisch mergen met je branch. Je gaat nadien als je je aanpassingen wil toevoegen wel eerst moeten mergen.

 **Pull:**

Hetzelfde als fetch, maar hierbij gaat GIT wil automatisch de commits mergen met je branch of tak. Deze actie doe je bijna standaard als je begint te werken aan iets nieuw. Je wil je branch/tak volledig updaten met mogelijke aanpassingen voordat je begint te werken aan je code.

 **Stash:**

Onafgewerkte code wil je liever niet committen. Je wil toch aan een andere feature werken op een andere branch. Dit kan je niet doen zonder te committen. Een oplossing hiervoor is stashen. Je gaat je 'vuile'code gaan opslaan, die je nadien terug kan opvragen. Deze code komt niet in je branch of master versie terecht.

 **Push:**

De aanpassingen die jij hebt gecommit kan je pushen naar de repository die online staat. In de praktijk ga je vaak inhoud van je eigen branch gaan mergen met de master, en deze nieuwe master-versie online pushen zodat die beschikbaar is voor anderen.

# 2. CSS-animaties #

Om de aandacht te trekken of te vestigen op een object op je webpagina kan je gebruik maken van CSS-animaties. Transities en animaties verschillen niet zoveel van elkaar. Je stelt de CSS properties in die moeten wijzigen, nadien bepaal je het verloop en duurtijd in. Dit kan zonder Javascript aan te spreken.

 **Transition:**

In het geval van transities ga je een animatie laten uitvoeren door te reageren op een verandering in een CSS property, bv. een :hover event. Je kan ook verschillende transities toepassen op verschillende klassen, en door middel van Javascript classes veranderen. Eigen aan een transitie is dat ze vaak maar één keer afgaat tenzij je opnieuw klikt of iets doet. Je kan dus niet loopen.

 **Animation:**

Animations vergen geen expliciete trigger. Ze beginnen bij het laden van bv de pagina. Met animaties heb je veel meer controle over de animatie. Je kan bv van punt A naar B, C, D, E gaan. Dit kan je doen door middel van keyframes. Je kan de animaties ook laten loopen.

 **Transform:**

Ik heb dit onderdeel apart gezet omdat je transform zowel bij transitions als animations kan gebruiken. Met transform kan je objecten gaan bewegen, roteren of vergroten etc.. Dit kan je ook met enkel transitions, maar vergt veel inspanning van je browser. Door transforms te gebruiken ga je de hardware van een gebruiker opdragen om de berekeningen uit te voeren.

# 3. Advanced Javascript #

Tijdens deze les leerden we zelf een javascript framework bouwen. Het meest bekende javascript frame work is JQuery. Door de opkomst van mobiele apps is er een grote vraag naar applicaties die snel en effecient werken. Deze mobiele apps zijn vaak zeer eenvoudig in gebruik en functionaliteit. Je hebt als gevolg geen volledig framework nodig, enkel stukken er van. Om die reden ga je het framework zelf nabouwen of een eigen framework schrijven. Dit om een verbetering in performance te krijgen.

 **Javascript:**

JavaScript is een veelgebruikte scripttaal om webpagina's interactief te maken en webapplicaties te ontwikkelen. Tegenwoordig wordt de taal ook meer en meer gebruikt in andere toepassingen zoals machines, frigo's etc..

 **Prototype:**

Elk Javascript object heeft een prototype. Dit kan je vergelijken met een soort klasse zoals in Java. Het prototype zelf is ook een object. Elk object dat je aanmaakt erft de eigenschappen en methoden van het prototype. Door middel van prototypes kan je zelf functies gaan schrijven om een framework te bouwen.

Bv. je wil de huidige datum in een variabele steken. Dit zal je vaak doen met var datum = Date(). Op deze manier creëer je een object die de eigenschappen en methoden erft van Date.prototype.

Belangrijk om te weten is dat er een soort hierarchie is binnen de prototypen. Object.prototype staat bovenaan deze hierarchie. Alle Javascript objecten erven eigenschappen van dit prototype.

# 4. API's #

Binnen de IT wereld en webdesign is het gebruik van API's bijna een standaard geworden. Bekende API's zijn die van Facebook, Twitter, Google Analytics etc.. Je gaat eigenlijk een verbinding maken met een applicatie van een ander bedrijf, en bepaalde functionaliteiten of data gaan gebruiken in je eigen app. Wij hebben gebruik gemaakt van de forecast.io API om data te verzamelen over het weer. Aan de hand van deze API konden we dan zelf een weerapplicatie maken.

 **API key:**

Veel API's vereisen dat je een profiel of account aanmaakt bij het bedrijf die de API aanbiedt. Veel bedrijven doen dit omdat ze op deze manier de gebruiker kunnen identificeren. Dit om misbruiken tegen te gaan. Een andere reden waarom ze gebruikt worden is om quota's bij te houden. Vaak staan er limieten op het gebruik van een API. Vanaf dat je een bepaalde grens overschrijdt, moet je betalen om hem verder te gebruiken. Bv. je weer-applicatie heeft dagelijks 2000 gebruikers die meerdere keren je app openen per dag.

 **JSON:**

JSON of Javascript Object Notation, is een gestandaardiseerd gegevensformaat. Het wordt hoofdzakelijk gebruikt voor uitwisseling van data tussen server en webapplicatie. Het is een alternatief voor XML.

JSON data kan soms zeer onoverzichtelijk zijn. Dit ondervond ik tijdens de ontwikkeling van de weerapplicatie. Een handige tool om hier mee om te gaan is POSTMAN. Je kan deze extensie downloaden in Google Chrome. Als je de link naar je JSON data hebt kan je die kopiëren in POSTMAN, de selectie op GET zetten, en nadien krijg je een zeer overzichtelijke pagina met alle data.

# 5. Node.js #

Node.js was een zeer moeilijk les om te volgen. Dit kwam voornamelijk door het ontbreken van goede voorbeelden en ingewikkelde documentatie. De voornaamste dingen die ik geleerd heb tijdens deze les is het gebruik van de NPM installer. Node.js is modulair opgebouwd en bij de aanvang van je applicatie, maar ook erna ga je vaak eerst beginnen met bepaalde modules te downloaden door middel van de installer.

**Package.json:**

In deze file staan de dependencies voor je applicatie. Als je npm install gebruikt, gaat hij in deze file lezen welke modules hij allemaal moet installeren, alsook welke versie. Deze file is zeer belangrijk want als je projecten deelt met collega's, ga je vaak aanpassingen doen in deze file. Je collega's kunnen nadien zeer snel de modules installeren door de aangepaste file uit te lezen. Op deze manier moet je geen plugins/modules gaan doorsturen naar elkaar.

**Websockets:**

Websockets dienen om een connectie te maken tussen de client of browser en de server waarmee deze verbonden is. Deze connectie is bi directionaal, wat betekent dat beiden met elkaar kunnen communiceren. Websockets worden voornamelijk gebruikt voor live toepassingen, zoals bv een chatbox. Verschillende gebruikers in verschillende clients/browsers op verschillende computers krijgen allen live een update of oproep binnen als iemand een actie uitvoert. Bv. een bericht schrijven.

**Nadelen:**
Ik vind het gebruik van Node.js en MongoDB zeer omslachtig. De opstelling neemt veel tijd en is ingewikkeld. Ik vind persoonlijk dat er een zeer hoge drempel is waar beginners over moeten geraken vooraleer ze echt beginnen te coderen.

Verder werkt het gebruik van de modules en het genereren van code zeer verwarrend. Je krijgt zeer snel een honderdtal regels code die je niet meteen begrijpt. Dit verhoogt nogmaals de drempel.

# 6. AngularJS #

Deze les was zeer aangenaam om te volgen en zeer interessant. Het is een open source webapplicatie framework uitgebracht door Google. Het is zeer handig om te gebruiken in one-page applicaties. Het maakt gebruikt van het model-view-controller (MVC) patroon.

 **Directives:**

Door middel van directives kan je je eigen tags of attributen gaan aanmaken. Dit geeft je meer controle hoe je je HTML en DOM wil schrijven.

# 7. SASS & BEM #

SASS is een extensie op CSS3. SASS is een CSS preprocessor taal. Dit wil zeggen dat je je SASS taal of code gaat omzetten naar een CSS bestand nadat je klaar bent met je aanpassingen. Hierdoor lijkt de taal een beetje op PHP. Binnen SASS kan je gebruik maken van variabelen, elementaire wiskunde operators, nesting, mixins etc.

Het voornaamste voordeel aan SASS is dat je functies kan gebruiken, die de eigenschappen van verschillende prefixes gaan aanpassen. In het verleden was het moeilijk om zeer uitgebreide CSS te gaan onderhouden. Als je bv één kleur wilde aanpassen, dan moest je dit x-aantal keer doen. Door middel van functies en variabelen kan je de kleur één keer aanpassen binnen de functie zelf, en zo heel je stylesheet aanpassen waar nodig.

**BEM:**

Deze front-end methodologie is meer object georiënteerd dan SASS of basis CSS. BEM staat voor Blocks, Element en Modifier. Je kan een block vergelijken met een unorderded list, waarin verschillende li's zitten, in dit geval de elements.

# 8. Gulp #

Gulp is een build tool in webontwikkeling. Gulp zorgt voor de automatisering van veel voorkomende taken tijdens het werkproces. Gulp is geschreven is Javascript, en is dus zeer geschikt voor het gebruik bij front-end taken. Je kan bijvoorbeeld instellen dat je LESS of SASS files automatisch omgezet worden naar CSS, telkens dat je een aanpassing uitgevoerd hebt. Je kan je CSS of Javascript files gaan minimizen.

Gulp op zich doet niet heel veel, maar er zijn zeer veel plugins beschikbaar. Gulp is gebaseerd op Node.js, en je moet plugins installer door middel van de npm-installer.

### Contact ###

Lucas Poignonnec
2IMDA 2014-2015
r0464122@student.thomasmore.be
lpoignonnec@gmail.com